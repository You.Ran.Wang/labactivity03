// YOU RAN WANG
package application;
import vehicles.Bicycle;
class BikeStore{
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("John Cena", 69, 420);
        bicycles[1] = new Bicycle("Joe Biden", 70, 421);
        bicycles[2] = new Bicycle("Donald Trump",71, 422);
        bicycles[3] = new Bicycle("Barack Obama", 72, 423);

        for(Bicycle bike : bicycles){
            System.out.println(bike);
        }
    }
}